## Sobre o Reino Plantae
O Reino Plantae, Vegetal ou Metaphyta abrange todas as plantas, as quais são seres eucariontes e pluricelulares. Quase todos os seus representantes possuem cloroplastos com clorofila, podendo realizar fotossíntese, sendo então denominados autótrofos por produzirem seus próprios alimentos. Suas células possuem parede celular de celulose e armazenam amido como substância de reserva. Além disso, as plantas são seres aeróbicos por precisarem de oxigênio para a produção de energia através das mitocôndrias.\
As semelhanças entre as plantas e as algas verdes sugerem que esse último grupo possa ter ancestrais que deram origem aos vegetais. Uma grande marca foi a grande capacidade adaptativa na conquista gradual e extensa do meio terrestre pelas plantas, que deixaram o ambiente aquático e por isso acabaram desenvolvendo estruturas e mecanismos especiais capazes de superar problemas como a perda de água para o ar. Apesar dessas novas características, houveram semelhanças que se mantiveram, como a presença de parede celular feita de celulose; a clorofila, pigmento responsável pela fotossíntese e ambas possuírem células reprodutivas com flagelos, além de armazenarem amido no interior dos cloroplastos.\
Os seres vivos deste reino possuem duas fases, a fase em que produzem gametas, com células haploides e a fase em que produzem esporos, com células diploides. Quanto mais evoluída a planta, maior é sua fase esporofítica e quanto menor sua evolução, maior é sua fase gametofítica, sendo este processo conhecido por alternância de gerações.\
O Reino Vegetal em geral são seres eucariontes clorofilados, porém é necessário definir outros critérios que possibilitem a classificação das plantas para organizá-las em grupos menos abrangentes que o reino, estes são seus filos que são separados por determinadas características evolutivas, como as briófitas que não possuem vasos condutores de seiva e por isso são classificadas como avasculares, já as pteridófitas e os grupos posteriores são agrupados como vegetais vasculares.\
Um dos critérios que classificam as plantas são a presença de estruturas reprodutivas escondidas (sem sementes) ou aparentes (com flores, frutos ou sementes). Os grupos em que estas estruturas são ocultas são chamadas de Criptógramas, sendo seus representantes as briófitas e pteridófitas, já as Gimnospermas e Angiospermas são grupos em que esta estrutura é visível, conhecidos também como Fanerógramas.
### Organograma do Reino Plantae
![organograma](organograma.png)
### Alternância de Gerações
A alternância de gerações ou ciclo haplodiplobionte é uma forma de reprodução
encontrada em todas as plantas e nos celenterados/cnidários. Nesses organismos
ocorre uma reprodução por via assexuada (não ocorre troca de gametas) e uma fase
sexuada (ocorre troca de gametas).
Em botânica chama-se alternância de gerações ao ciclo de vida de muitas plantas
e algas que apresentam duas formas multicelulares diferentes para a fase haplóide,
o gametófito, e para a fase diplóide, o esporófito. Utiliza-se aqui o termo "plantas" no
sentido da taxonomia de Lineu, ou seja, incluindo as plantas verdes, os fungos e as
algas.
