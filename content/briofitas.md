---
title: "Briofitas"
date: 2022-09-29T16:07:02-03:00
draft: false
---

As briófitas foram as primeiras plantas a ocuparem o ambiente terrestre e por esse motivo possuem as estruturas mais simples do Reino Plantae. No geral, elas possuem um caulóide (um “caule simples”), no qual se prendem muitos filoides pequenos, finos e simples que absorvem água diretamente do meio. São organismos autótrofos e de pequeno porte, chegando no máximo até cerca de vinte centímetros pois são avasculares, ou seja, não possuem vasos condutores de seiva. Assim, o transporte de água é muito lento (tanto por difusão quanto por osmose), limitando o seu tamanho. Por não possuírem estruturas adequadas para evitar a transpiração, ressecando com facilidade, elas habitam locais úmidos e de baixa luminosidade.\
Os principais representantes das briófitas são os musgos, por serem mais comuns no nosso cotidiano. Apresentando 90.000 espécies já classificadas, estes geralmente cobrem extensas áreas de solo úmido, rochas e troncos de árvores e são formados por um rizóide, caulóide e filóide. Rizóides são filamentos responsáveis pela fixação e pela absorção da água e dos sais minerais do ambiente; caulóide é uma pequena haste de onde partem os filóides; e os filóides são estruturas clorofiladas capazes de fazer fotossíntese.
### Subdivisões
Subdivisões
As briófitas são divididas em três classes:
- Classe Musci: vegetais que apresentam o corpo dividido em três regiões específicas rizóide, caulóide, e filóide, da qual os musgos fazem parte
- Classe Hepaticae: vegetais cujo a forma do gametófito têm forma de fígado, classe na qual fazem parte as hepáticas (plantas com formas achatadas)
- Classe Anthocerotae: vegetais que medem cerca de 2 cm e sao presos ao substrato por rizóides e seu gametófito é folhoso, arredondado, e multilobado. É a classe na qual fazem parte os antóceros.
### Estrutura dos musgos
![estrutura_musgo](estrutura_musgo.png)
### Reprodução dos musgos
Localizados no ápice dos gametófitos, existem órgãos especializados na produção de gametas chamados de gametângios. O masculino chama-se anterídio e os gametas anterozóides (seres flagelados), enquanto a feminino, arquegônio que produz apenas um gameta feminino, a oosfera. \
Para a fecundação ocorrer, os anterozóides são liberados pelo anterídio e deslocam-se, por meio de um ambiente necessariamente aquoso, em direção ao arquegônio, em seguida, fecundando a oosfera. Sendo assim, forma-se o zigoto (diplóide, 2n) que, por mitose, origina o embrião. Esse desenvolve-se no interior do arquegônio e transforma-se num esporófito. \
O jovem esporófito, no seu crescimento, rompe o arquegônio e carrega em sua ponta dilatada um pedaço rompido do arquegônio, em forma de "boné", conhecido como caliptra. Já como adulto, o esporófito, apoiado no gametófito feminino, é formado por uma haste e, na ponta, uma cápsula (que é um esporângio) dilatada, dotada de uma tampa, coberta pela caliptra. \
No esporângio, células 2n sofrem meiose e originam esporos haploides. Para serem liberados, é preciso inicialmente que a caliptra seque e caia. A seguir, cai a tampa do esporângio. Em tempo seco e, preferencialmente, com vento os esporos são liberados e dispersam-se. Caindo em locais úmidos, cada esporo germina e origina um filamento semelhante a uma alga, o protonema. Do protonema, brotam alguns musgos, todos idênticos geneticamente e do mesmo sexo. Outro protonema, formado a partir de outro esporo, originará gametófitos do outro sexo e, assim, completa-se o ciclo. Note que a determinação do sexo ocorre, então, já na formação dos esporos. \
![ciclo_briofitas](ciclo_briofitas.png)