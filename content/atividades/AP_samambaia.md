---
title: "Atividade Pratica - Samambaia"
date: 2022-09-29T17:02:58-03:00
draft: false
---

### 1. Objetivo
Analisar o conteúdo dos soros das samambaias, uma pteridófita, localizados na parte inferior da folha.

### 2. Materiais Utilizados
- Folha de samambaia com soros;
- Fita adesiva transparente;
- Tesoura;
- Lâmina;
- Microscópio óptico;

### 3. Procedimento
Primeiramente retira-se um pedaço de fita adesiva transparente, usando-a, coleta-se o material do soro da folha de samambaia delicadamente, cola-se a fita adesiva com material na lâmina e posiciona-se a lâmina no microscópio óptico

### 4. Esquemas 
![cebola](images/cebola.png "cebola")

### 5. Conclusão
     Por meio deste experimento, pode-se observar os esporângios que se localizam no interior dos soros. Essas estruturas são responsáveis pelo produção dos esporos que se dá pela meiose. Quando maduras, rompem-se liberando os esporos que se espalham por conta do vento, permitindo assim o desenvolvimento do gametófito, no caso das samambaias, o prótalo.