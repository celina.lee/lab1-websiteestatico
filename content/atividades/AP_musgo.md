---
title: "Atividade Pratica - Musgo"
date: 2022-09-29T17:02:58-03:00
draft: false
---

### 1. Objetivo
Analisar as estruturas do musgo, uma briófita, em uma determinada fase do seu ciclo de vida.

### 2. Materiais Utilizados
- Musgo;
- Pinça;
- Lâmina;

### 3. Procedimento
Primeiramente retira-se uma amostra de musgo, colocando a  na lâmina e em seguida, posiciona-se a lâmina no microscópio óptico para observação.

### 4. Esquemas
![cebola](images/cebola.png "cebola")

### 5. Conclusão
Através do experimento pudemos observar detalhadamente as partes que compõem o musgo. Na parte inferior, pode-se observar os rizóides, pequenos filamentos que fixam a planta no solo e absorvem água e sais minerais disponíveis. No meio, o caulóide, onde fixam-se os filóides, estruturas que possuem estruturas finas e clorofiladas, capazes de fazer fotossíntese. Essas três partes fazem parte do gametófito(n) da planta. Há, também, a presença da haste e da cápsula, estruturas as quais constituem o esporófito(2n) do musgo.