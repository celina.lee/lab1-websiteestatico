---
title: "Atividade Pratica - Pinheiro"
date: 2022-09-29T17:02:58-03:00
draft: false
---

### 1. Objetivo 
     Tem-se como objetivo o aprofundamento do estudo sobre as gimnospermas através da observação do estróbilo masculino, estróbilo feminino e a semente através do grão de pólen (gametófito masculino), esporófila (folha reprodutora encontrada na pinha) e o pinhão.

### 2. Materiais Utilizados
- Pinha;
- Estróbilo masculino;
- Pinhão;
- Pinça;
- Conta gotas;
- Estilete;
- Lâmina;
- Microscópio óptico;

### 3. Procedimento 
- Primeiramente retira-se cuidadosamente a esporófila da pinha com a utilização de uma pinça, coloca-se o material a ser analisado na lâmina, adiciona-se uma gota d’água sobre o material e posiciona-se a lâmina no microscópio óptico.
- Primeiramente pressiona-se delicadamente o estróbilo masculino para obtenção de grãos de pólen, coloca-se o material a ser analisado na lâmina, adiciona-se uma gota d’água sobre o material recolhido e posiciona-se a lâmina no microscópio óptico.
- Primeiramente retira-se a casca exterior do pinhão, obtendo apenas a parte de dentro da semente, executa-se um corte longitudinal no pinhão, retira-se o embrião que se encontra na parte interna da semente, coloca-se o material a ser analisado na lâmina e posiciona-se a lâmina no microscópio óptico.

### 4. Esquemas 
![cebola](images/cebola.png "cebola")

### 5. Conclusão 
     Neste experimento conhecemos melhor algumas estruturas de reprodução do pinheiro, tais como o estróbilo masculino (grão de pólen), estróbilo feminino (megásporos) e também o próprio embrião. Quando um estróbilo masculino se abre e libera grande quantidade de grãos de pólen, esses grãos se espalham no ambiente e podem ser levados pelo vento até o estróbilo feminino onde é introduzido o núcleo espermático, este fecunda a oosfera, gerando o zigoto. \
     O estróbilo masculino produz pequenos esporos chamados grãos de pólen. O grão de pólen de forma geral, são pequenos, arredondados, alguns alados, e são produzidos por meiose no microsporângio. Normalmente são revestidos por paredes de celulose ornamentadas, característica de cada família ou mesmo auxiliando na identificação das espécies de plantas. \
     O estróbilo feminino produz estruturas denominadas óvulos. No interior de um óvulo maduro surge um grande esporo, e a produção de sementes ocorre no estróbilo feminino. O embrião é uma estrutura originária da fertilização de um óvulo (gameta feminino) por espermatozóide (gameta masculino). Logo após a fertilização, a estrutura gerada passa a ser chamada de zigoto.