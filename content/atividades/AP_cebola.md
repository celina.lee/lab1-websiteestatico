---
title: "Atividade Pratica - Cebola"
date: 2022-09-29T17:02:42-03:00
draft: false
---

### 1. Objetivo
     O experimento tem como objetivo a observação da epiderme da cebola, uma angiosperma.

### 2. Materiais Utilizados
- Cebola;
- Estilete;
- Lâmina;
- Lamínula;
- Conta gotas;

### 3. Procedimento
Com auxílio do estilete, corta-se longitudinalmente a cebola, retira-se com cuidado sua epiderme, coloca-a na lâmina e pôe-se gotículas de água por cima do experimento. Então, em um ângulo de 45° posiciona-se a lamínula para a observação do catáfilo por meio do microscópio.

### 4. Esquemas
![cebola](cebola.png)

### 5. Conclusão 
     Neste experimento aprendemos mais sobre a epiderme da cebola e o catáfilo, folha reduzida que geralmente protege as gemas dormentes, com grandes reservas de nutrientes. Contudo nas cebolas, atuam como órgão de reserva. \
     A epiderme, que reveste os catafilos, não possuem clorofila, são vivas, achatadas e com células bastante justapostas, ou seja, bastante próximas uma das outras. As linhas extremamente finas observadas entre cada célula são as paredes celulares, encontradas nas células vegetais, na qual tem a função de estruturação da célula, oferecendo sustentação e proteção. Além disso, apresenta relação com a absorção, transporte e secreção de substâncias. A parede celular vegetal é formada pela membrana esquelética celulósica, que é constituída exclusivamente por ligações de moléculas de glicose, responsável por garantir sua arquitetura, sendo assim visível ao microscópio óptico comum.  