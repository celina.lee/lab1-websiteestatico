---
title: "Gimnospermas"
date: 2022-09-29T16:07:02-03:00
draft: false
---

Gimnospermas é uma subdivisão do Reino Plantae, cujas plantas são vasculares e possuem raiz, caule, e folhas. O termo “gimnosperma” vem do grego gymnos = nu e sperma = semente, ou seja, significa semente nua devido à ausência de frutos nessas plantas. Uma das características desse filo é a existência de estróbilos ou cones, uma estrutura reprodutora com um conjunto de esporângios protegidos em folhas em forma de escamas.\
As Gimnospermas pertencem ao grupo das fanerógamas (com flores) e espermatófitas (com sementes) e ainda possuem o mesmo método de transporte de seiva das pteridófitas. São dotadas de folhas longas e finas (acículas) ou curtas e espessas em forma de escamas, tronco espesso com muitos galhos. As flores desses vegetais são atípicas e reúnem-se em estruturas denominadas estróbilos ou cones.\
Este grupo tem grande importância como fornecedores de matéria-prima, madeira, celulose, resinas, medicamentos, gomas, essências, além de algumas sementes servirem de alimento.\
A transição das pteridófitas para gimnospermas trouxe muitas mudanças para este novo grupo, a aquisição evolutiva que caracteriza as espermatófitas é a semente, que não existia nas pteridófitas. Nas espermatófitas, a germinação dos megásporos ocorre no interior dos megasporângios, que passam a ser chamados óvulos. Em cada megasporângio, três dos quatro megásporos, formados por meiose, se atrofiam e um se desenvolve, ocupando todo o interior desse óvulo. A fecundação não depende de água do meio externo, mas apenas de líquidos secretados pelo próprio megasporângio, onde vão nadar os anterozoides pluri flagelados.
### Subdivisões
Elas são distribuídas em quatro grupos:
- Conipherophyta (ou Pinophyta): Seus representantes são os pinheiros, as araucárias (pinheiro-do-paraná), as sequóias, ciprestes e plantas semelhantes. As folhas das coníferas normalmente são finas e longas, em formato de agulha.
- Cycadophyta: As cicas (cicadófita) são plantas que se assemelham com as
palmeiras, pois suas folhas se concentram principalmente no ápice do caule e
apresentam folhas coriáceas. O tronco é grosso e macio, composto por uma grande
massa de medula.
- Gnetophyta: Este filo é um pequeno grupo de gimnospermas, abrigado 70
espécies, distribuídas em 3 gêneros: Gnetum, Ephedra eWelwitschia. Possuem
semelhanças com angiospermas, principalmente em seu sistema vascular.
- Ginkgophyta: as folhas possuem formato de leque e caem no inverno. Antes de
caírem, as folhas ficam douradas. Possuem crescimento lento. O único representante
vivo deste grupo é o Ginkgo biloba.
### Reprodução dos pinheiros
Há dois tipos de estróbilos: o masculino (microstróbilo), e o feminino (megastróbilo ou macrostróbilo), sendo em ambos produzido esporos em seu interior por meiose. \
Nos microstróbilos (n) formam-se os micrósporos (n) na qual ao sofrerem mitose cada um gera um gametófito masculino também chamados de prótalo ou grão de pólen. Dentro de cada megastróbilo, há vários esporângios conhecidos por óvulos imaturos, nos quais ocorre meiose, gerando quatro células haploides como dito anteriormente; três delas degeneram, restando apenas a célula do megásporo (n). O óvulo imaturo é constituído pelo megásporo e seu envoltório, o tegumento (2n). \
No interior do óvulo, o megásporo sofre mitoses e origina o gametófito feminino (n), também denominado megaprótalo ou saco embrionário. O gametófito feminino tem inúmeras células de menor tamanho (sem função reprodutiva) e possui algumas células grandes, as oosferas (gametas femininos). Constituem um arquegônio rudimentar as células que rodeiam cada oosfera. Em resumo, um óvulo maduro tem os seguintes componentes: um tegumento (2n) e um gametófito feminino, que possui algumas oosferas (gametas femininos). \ 
Ao atingir a micrópila (abertura do óvulo), o grão de pólen cresce lentamente no interior do gametófito feminino e desenvolve-se formando o tubo polínico. O pinheiro não apresenta anterozóides flagelados e sua fecundação não depende da água, sendo adaptativa ao ambiente terrestre. Essa fecundação depende do crescimento do tubo polínico é denominada sifonogamia. \
O tubo polínico corresponde ao gametófito masculino maduro, e dois de seus núcleos correspondem aos gametas masculinos, sendo denominados núcleos gaméticos, ou núcleos espermáticos. A fecundação resulta na formação da semente (pinhão), que é o óvulo fecundado e desenvolvido. O zigoto (2n) formado sofre mitose e gera um embrião (2n). O gametófito feminino (n), no interior do qual o embrião se desenvolve, passa a atuar como reserva nutritiva, constituindo o endosperma haploide. \
O óvulo tem apenas um tegumento, que persiste como o tegumento da semente (2n). Com a germinação da semente, ocorre a formação de um novo esporófito. \
![reproducao_pinheiro](reproducao_pinheiro)
