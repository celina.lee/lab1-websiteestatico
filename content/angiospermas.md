---
title: "Angiospermas"
date: 2022-09-29T16:07:02-03:00
draft: false
---

As angiospermas (angio = urna; sperma = semente) são as plantas mais presentes no nosso cotidiano pelo fato de que esse grupo é o mais evoluído dentro do reino Plantae, apresentando duas características exclusivas: as flores e os frutos, que são fundamentais para o desenvolvimento das mesmas. Possuem também raiz, caule, folhas e sementes. São mais de 250 mil espécies em diversos tamanhos e formas, além de crescerem em diversos tipos de ambientes.
### Subdivisões
As Angiospermas classificam-se em dois grupos, Monocotiledôneas e Dicotiledôneas. O nome é devido ao número de cotilédones (folha nutritiva embrionária) presente na semente. Ou seja, as monocotiledôneas (grama, milho, orquídea) apresentam apenas um cotilédone na semente enquanto que as dicotiledôneas (quase que a totalidade das angiospermas) possuem dois cotilédones na semente. 
#### Monocotiledoneas
- Gramíneas: cereais como: trigo, cevada, arroz e aveia, além de gramas, capins, bambus e cana-de-açúcar e milho
- Palmáceas: caules de um só eixo(estipe) e com grandes cachos de frutos. Exemplos: coco-da-baia, dende, açai palmito tamareira, carnaúba, buriti , jerivá e piaçava.
#### Dicotiledôneas
- Asteráceas(compostas): árvores e arbustos com inflorescências típicas. É a maior família de dicotiledôneas, apresentando mais de 23 mil espécies. Exemplos: dália, girassol, margarida, crisântemo, alface, camomila, chicória, alcachofra,.
- Fabáceas (leguminosas): ervas e árvores com fruto alongado, o legume, e sementes dispostas em fila. Exemplos: feijão, lentilha, grão-de-bico, pata-de-vaca, pau-brasil, jatoba.
#### Diferenças
Na tabela a seguir temos a explicação detalhada das principais diferenças entre dicotiledôneas e monocotiledôneas: \
![mono_dicotiledonea](mono_dicotiledonea.jpg)
### Estrutura das Angiospermas
- Raiz
- Caule
- Folha
- Flor
- Semente
- Fruto
